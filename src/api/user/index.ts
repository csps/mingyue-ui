import request from '/@/utils/request';

/**
 * 用户 api 接口集合
 * @method userInfo 用户信息
 */
export function userApi() {
    return {
        userInfo: () => {
            return request({
                url: '/api/system/sysUser/info',
                method: 'get'
            });
        },
        list: (page: object) => {
            return request({
                url: '/api/system/sysUser/list',
                method: 'get',
                params: page
            });
        },
        addOrEdit: (data: object) => {
            return request({
                url: '/api/system/sysUser/addOrEdit',
                method: 'post',
                data: data
            });
        },
        del: (userId: number) => {
            return request({
                url: '/api/system/sysUser/' + userId,
                method: 'delete'
            });
        }
    };
}
