import request from '/@/utils/request';

/**
 * （不建议写成 request.post(xxx)，因为这样 post 时，无法 params 与 data 同时传参）
 *
 * 登录api接口集合
 * @method signIn 用户登录
 * @method signOut 用户退出登录
 */
export function useLoginApi() {
	return {
		signIn: (data: object) => {
			return request({
				url: '/api/auth/login',
				method: 'post',
				data
			});
		},
		smsLogin: (data: object) => {
			return request({
				url: '/api/auth/smsLogin',
				method: 'post',
				data
			});
		},
		emailLogin: (data: object) => {
			return request({
				url: '/api/auth/emailLogin',
				method: 'post',
				data
			});
		},
		signOut: () => {
			return request({
				url: '/api/auth/logout',
				method: 'delete'
			});
		},
		sendCode: (phone: string) => {
			return request({
				url: '/api/push/sms/code',
				method: 'get',
				params: {phone : phone}
			});
		},
		sendEmailCode: (email: string) => {
			return request({
				url: '/api/push/email/code',
				method: 'get',
				params: {email : email}
			});
		},
	};
}
