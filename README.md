<img src="https://strive-md-1258023676.cos.ap-nanjing.myqcloud.com/md/image-20230526094652332.png" alt="logo_副本" />

<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">MingYue-SpringCloud v1.5.0</h1>
<h4 align="center">🎉 基于 Vue3 + TS + Vite + Element plus 等技术，适配 MingYue-SpringCloud 后台微服务</h4>

#### 🌈 介绍

基于 vue3.x + CompositionAPI setup 语法糖 + typescript + vite + element plus + vue-router-next + pinia 技术，适配手机、平板、pc 的后台开源免费模板，希望减少工作量，帮助大家实现快速开发。

感谢开源项目：[vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)

#### 🚧 安装 cnpm、yarn

- 复制代码(桌面 cmd 运行) `npm install -g cnpm --registry=https://registry.npm.taobao.org`
- 复制代码(桌面 cmd 运行) `npm install -g yarn`

#### 🏭 环境支持

| Edge      | Firefox      | Chrome      | Safari      |
| --------- | ------------ | ----------- | ----------- |
| Edge ≥ 88 | Firefox ≥ 78 | Chrome ≥ 87 | Safari ≥ 13 |

> 由于 Vue3 不再支持 IE11，故而 ElementPlus 也不支持 IE11 及之前版本。

#### ⚡ 使用说明

建议使用 cnpm，因为 yarn 有时会报错。<a href="http://nodejs.cn/" target="_blank">node 版本 > 14.18+/16+</a>

> Vite 不再支持 Node 12 / 13 / 15，因为上述版本已经进入了 EOL 阶段。现在你必须使用 Node 14.18+ / 16+ 版本。

```bash
# 克隆项目
git clone git@gitee.com:csps/mingyue-ui.git

# 进入项目
cd mingyue-ui

# 强烈建议不要用直接使用 cnpm 安装，会有各种诡异的 bug，可以通过重新指定 registry 来解决 npm 安装速度慢的问题。
npm install --registry=https://registry.npmmirror.com

# 运行项目
npm run dev

# 打包发布
npm run build
```
